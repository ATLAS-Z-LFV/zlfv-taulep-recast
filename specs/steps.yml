ntuple_production:
  process:
    process_type: interpolated-script-cmd
    script: |
      /recast_auth/getkrb.sh
      source /home/atlas/release_setup.sh
      export PATH=/home/atlas/xrootd/bin:$PATH
      mkdir {outputdir}/input_DAODs
      mkdir {outputdir}/premerge_ntuples
      xrdcp -r {SIGNAL_DAOD} {outputdir}/input_DAODs
      if [ {PRW_FILE} != DEFAULT ] ; then
        xrdcp -f {PRW_FILE} /ntuple_production/source/LFV_Z/data/PRW/PRW_MC{MC_CAMPAIGN}.root
      fi
      cd /ntuple_production/build && source x*-opt/setup.sh
      cd /ntuple_production/run
      ./run.py --submitDir {outputdir}/submitDir \
      --sampleHandlingMode scanDir \
      --scanDepth 1 \
      --localPath {outputdir}/input_DAODs \
      --MC-campaign {MC_CAMPAIGN} \
      --maxEvents {MAX_EVENTS} \
      --mode LFVLep
      case {MC_CAMPAIGN} in
         "16a") RTAG=r9364
         ;;
         "16d") RTAG=r10201
         ;;
         "16e") RTAG=r10724
         ;;
      esac
      mv -v {outputdir}/submitDir/data-ntuple {outputdir}/premerge_ntuples/user.dummy.LFVZ.{SIGNAL_DSID}.{SIGNAL_NAME}.dummy_"$RTAG"_dummy.ntuple.root
      rm -rvf {outputdir}/input_DAODs
  publisher:
    publisher_type: interpolated-pub
    publish:
      PREMERGE_NTUPLES: '{outputdir}/premerge_ntuples'
    glob: true
  environment:
    environment_type: docker-encapsulated
    image: gitlab-registry.cern.ch/atlas-z-lfv/z-lfv-full-run2/ntuple_production
    imagetag: taulep-approval
    resources:
      - GRIDProxy

merge_ntuples:
  process:
    process_type: interpolated-script-cmd
    script: |
      /recast_auth/getkrb.sh
      source /home/atlas/release_setup.sh
      export PATH=/home/atlas/xrootd/bin:$PATH
      mkdir {outputdir}/premerge_ntuples
      mkdir {outputdir}/merged_ntuples
      ln -s {PREMERGE_NTUPLES_16a}/user.dummy.LFVZ.*.ntuple.root {outputdir}/premerge_ntuples/.
      ln -s {PREMERGE_NTUPLES_16d}/user.dummy.LFVZ.*.ntuple.root {outputdir}/premerge_ntuples/.
      ln -s {PREMERGE_NTUPLES_16e}/user.dummy.LFVZ.*.ntuple.root {outputdir}/premerge_ntuples/.
      if [ {XSEC_FILE} != DEFAULT ] ; then
        xrdcp {XSEC_FILE} {outputdir}/xsec_file.xml
        XSEC_ARGS="--xsecfile {outputdir}/xsec_file.xml"
      else
        XSEC_ARG=''
      fi
      cd /ntuple_production/build && source x*-opt/setup.sh
      cd /ntuple_production/merge
      ./mergeNtuples.py --multiple-revisions -i {outputdir}/premerge_ntuples -o {outputdir}/merged_ntuples/{SIGNAL_NAME}.root --process {SIGNAL_NAME} --prefix {SIGNAL_NAME} $XSEC_ARG
  publisher:
    publisher_type: interpolated-pub
    publish:
      MERGED_NTUPLES: '{outputdir}/merged_ntuples/{SIGNAL_NAME}.root'
    glob: true
  environment:
    environment_type: docker-encapsulated
    image: gitlab-registry.cern.ch/atlas-z-lfv/z-lfv-full-run2/ntuple_production
    imagetag: taulep-approval
    resources:
      - GRIDProxy

split_ntuples:
  process:
    process_type: interpolated-script-cmd
    script: |
      mkdir {outputdir}/split_ntuples
      cd /analysis
      case {CHANNEL} in
         "muel")
         REGIONS=SR_mutaulepe,CRTop_muel
         ;;
         "elmu")
         REGIONS=SR_eltaulepmu,CRTop_elmu
         ;;
      esac
      ./splitRegionTrees.py -i {MERGED_NTUPLES} -o {outputdir}/split_ntuples --thinning-db configs/thinning_HIGG4D1.yaml -R configs/regions_HIGG4D1.yaml -r "$REGIONS"
  publisher:
    publisher_type: interpolated-pub
    publish:
      SPLIT_NTUPLES: '{outputdir}/split_ntuples/{SIGNAL_NAME}.root'
    glob: true
  environment:
    environment_type: docker-encapsulated
    image: gitlab-registry.cern.ch/atlas-z-lfv/z-lfv-full-run2/nn_and_fit
    imagetag: taulep-approval

decorate_ZpT_weights:
  process:
    process_type: interpolated-script-cmd
    script: |
      mkdir {outputdir}/ZpT_weights
      mkdir {outputdir}/split_ntuples
      ln -s {SPLIT_NTUPLES} {outputdir}/split_ntuples/.
      cd /analysis
      case {CHANNEL} in
         "muel")
         FAKES=el
         ;;
         "elmu")
         FAKES=mu
         ;;
      esac
      sed -i "3s#.*#input_dir: {outputdir}/split_ntuples/#" configs/inputs_HIGG4D1_syst_split_"$FAKES"fakes.yaml
      ./decorateZPtSFs.py -I configs/inputs_HIGG4D1_syst_split_"$FAKES"fakes.yaml -o {outputdir}/ZpT_weights --Zll-sf-hist '' --Ztt-sf-hist ''
  publisher:
    publisher_type: interpolated-pub
    publish:
      ZPT_WEIGHTS: '{outputdir}/ZpT_weights/{SIGNAL_NAME}.root.ZWeights.friend'
    glob: true
  environment:
    environment_type: docker-encapsulated
    image: gitlab-registry.cern.ch/atlas-z-lfv/z-lfv-full-run2/nn_and_fit
    imagetag: taulep-approval

decorate_NN_outputs:
  process:
    process_type: interpolated-script-cmd
    script: |
      mkdir {outputdir}/NN_outputs
      cd /NN_studies
      case {CHANNEL} in
         "muel")
         TRAIN_REGION=SR_mutaulepe
         REGIONS=SR_mutaulepe,CRTop_muel
         ;;
         "elmu")
         TRAIN_REGION=SR_eltaulepmu
         REGIONS=SR_eltaulepmu,CRTop_elmu
         ;;
      esac
      ./decorateNN_binary.py --mode lep -i {SPLIT_NTUPLES} -o {outputdir}/NN_outputs --model-dir saved_models -t v38_{CHANNEL} -T {CHANNEL}_basic_dAlpha_mcoll --parallel --max-threads 8 --training-region "$TRAIN_REGION" --backgrounds-lep Ztautau,top,Di-Boson -r "$REGIONS"
  publisher:
    publisher_type: interpolated-pub
    publish:
      NN_OUTPUTS: '{outputdir}/NN_outputs/{SIGNAL_NAME}.root.NN.v38_{CHANNEL}_{CHANNEL}_basic_dAlpha_mcoll.friend'
    glob: true
  environment:
    environment_type: docker-encapsulated
    image: gitlab-registry.cern.ch/atlas-z-lfv/z-lfv-full-run2/nn_and_fit
    imagetag: taulep-approval

fit:
  process:
    process_type: interpolated-script-cmd
    script: |
      mkdir {outputdir}/split_ntuples
      ln -s {SPLIT_NTUPLES} {outputdir}/split_ntuples/.
      ln -s {NN_OUTPUTS} {outputdir}/split_ntuples/.
      ln -s {ZPT_WEIGHTS} {outputdir}/split_ntuples/.
      cd /analysis
      mkdir /analysis/data
      case {CH} in
         "muel")
         CON_VARS=n_muons_fit,el_mu_mcoll_fit
         FAKES=el
         ;;
         "elmu")
         CON_VARS=n_electrons_fit,mu_el_mcoll_fit
         FAKES=mu
         ;;
      esac
      sed -i "3s#.*#input_dir: {outputdir}/split_ntuples/#" configs/inputs_HIGG4D1_syst_split_"$FAKES"fakes.yaml
      ./fit_multFitRegions.py --tag recast_results \
      -I configs/inputs_HIGG4D1_syst_split_"$FAKES"fakes.yaml \
      --NN-tag v38_{CH}_{CH}_basic_dAlpha_mcoll \
      --backup-cache cached_bkg_hists/syst_{CH}.root \
      --fit-region SR_{CH}_low,SR_{CH}_high --fit-variable NN_comb_new \
      --con-region CRTop_{CH}_tight,CRZtt_{CH} \
      --con-variable "$CON_VARS"  \
      --force-unblind --syst \
      --regions-db configs/regions_HIGG4D1.yaml \
      --histograms-db configs/histograms_HIGG4D1.yaml \
      --systematics-db configs/systematics_HIGG4D1.yaml \
      --plots-db configs/plots_HIGG4D1.yaml \
      --no-split-prongs \
      {FIT_ARGS}
      mv /analysis/results/fit_recast_results {outputdir}/recast_results
      cd {outputdir}/recast_results
      /analysis/python_macros/fitLogToYAML.py       fit_*.log           BestFitPOI.yaml
      /analysis/python_macros/hypoTestLogToYAML.py  DiscHypoTest_*.log  DiscHypoSig.yaml
      /analysis/python_macros/ULScanLogToYAML.py    ULScan_*.log        ULs.yaml
  publisher:
    publisher_type: interpolated-pub
    publish:
      fit_results: '{outputdir}/recast_results'
    glob: true
  environment:
    environment_type: docker-encapsulated
    image: gitlab-registry.cern.ch/atlas-z-lfv/z-lfv-full-run2/nn_and_fit
    imagetag: taulep-approval
